let text = document.getElementById("title");
const button = document.getElementById("button");
let contentAllcards = document.getElementById("content-card");

button.addEventListener("click", listCard);

// localStorage.clear();

// function to store the cards
async function listCard() {
  const randomUrl = Math.floor(Math.random() * (6 - 1) + 1);
  const randomPlanets = Math.floor(Math.random() * (61 - 1) + 1);
  const randomSpecies = Math.floor(Math.random() * (39 - 1) + 1);
  const randomStarships = Math.floor(Math.random() * (37 - 1) + 1);
  const randomPeoles = Math.floor(Math.random() * (84 - 1) + 1);
  const randomVehicule = Math.floor(Math.random() * (40 - 1) + 1);
  let url;

  switch (randomUrl) {
    case 1:
      url = `https://swapi.dev/api/planets/${randomPlanets}`;
      break;
    case 2:
      url = `https://swapi.dev/api/starships/${randomStarships}`;
      break;
    case 3:
      url = `https://swapi.dev/api/people/${randomPeoles}`;
    case 4:
      url = `https://swapi.dev/api/vehicles/${randomVehicule}`;
    case 5:
      url = `https://swapi.dev/api/species/${randomSpecies}`;
    default:
      url = `https://swapi.dev/vehicles/${randomUrl}`;
      break;
  }
  const response = await fetch(url);
  const data = await response.json();
  // if (data.detail === "Not found") {
  //   continue;
  // }
  console.log("promise", data);
  window.localStorage.setItem(data.name, JSON.stringify(data));
  document.getElementById("card-title").innerHTML = `Name: ${data.name}`;
  document.getElementById("card-content").innerHTML = `<p>Url: ${data.url}</p>`;
}

function showAllCards() {
  var archive = [],
    keys = Object.keys(localStorage),
    i = 0,
    key;
  let numbercard = 0;

  for (; (key = keys[i]); i++) {
    archive.push(JSON.parse(localStorage.getItem(key)));
    numbercard++;
  }
  contentAllcards.innerHTML = "";
  for (i = 0; i < archive.length; i++) {
    contentAllcards.innerHTML += `<div class="card" style="width: 18rem">
                                    <div class="card-body">
                                  <h5>${archive[i].name}</h5>
                                  </div>
                                  <p>Url: ${archive[i].url}</p>
                                </div>`;
  }
  console.log(numbercard);
}
showAllCards();

function chronotime() {
  const now = new Date().getTime();
  const countDownTime = new Date("February 5,2022").getTime();

  // console.log("Heure:", countDownTime);

  const distanceBase = countDownTime - now;
  const days = Math.floor(distanceBase / (1000 * 60 * 60 * 24));

  const hours = Math.floor(
    (distanceBase % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  );
  const minutes = Math.floor((distanceBase % (1000 * 60 * 60)) / (1000 * 60));

  const seconds = Math.floor((distanceBase % (1000 * 60)) / 1000);

  // text.innerText = `${hours}h ${minutes}m ${seconds}s`;
}

setInterval(chronotime, 1000);
